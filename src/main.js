/**
 * This template is a production ready boilerplate for developing with `PuppeteerCrawler`.
 * Use this to bootstrap your projects using the most up-to-date code.
 * If you're looking for examples or want to learn more, see README.
 */

const Apify = require('apify');

const { utils: { log } } = Apify;

Apify.main(async () => {

    // Setting up the starting point
    const { keyword } = await Apify.getInput();
    log.info("Starting crawler with keyword: " + keyword);
    const startUrl = `https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=${keyword}`
    const requestList = await Apify.openRequestList('start-url', [{
        url: startUrl,
        userData: { label: "START" },
    }]);

    // We'll also use a request queue later
    const requestQueue = await Apify.openRequestQueue();

    // const proxyConfiguration = await Apify.createProxyConfiguration();
    const crawler = new Apify.PuppeteerCrawler({
        requestList,
        requestQueue,
        // proxyConfiguration,
        useSessionPool: true,
        persistCookiesPerSession: true,
        launchPuppeteerOptions: {
            // Chrome with stealth should work for most websites.
            // If it doesn't, feel free to remove this.
            useChrome: true,
            stealth: true,
        },
        handlePageFunction: async (context) => {
            const { url, userData: { label } } = context.request;
            const request = context.request;
            log.info('Page opened.', { label, url });
            const page = context.page;
            const baseURl = 'https://www.amazon.com';

            // START: fetch all ASIN of the products found
            if (request.userData.label === "START") {

                // Get all the products ASIN
                // const ASINList = $('div[data-asin]').map((i, elem) => $(elem).attr('data-asin')).get();
                let ASINList = await page.$$eval('div[data-asin]', (els) => els.map((el) => el.getAttribute('data-asin')));
                log.info('Found ASIN:' + ASINList);
                for (const ASIN of ASINList) {

                    // Filter blank ASINs
                    if (ASIN) {
                        let url = new URL(`https://www.amazon.com/dp/${ASIN}`, baseURl)
                        log.info('Enqueuing product URL:', { href: url.href });
                        await requestQueue.addRequest(
                            {
                                url: url.href,
                                userData: {
                                    label: "DETAIL",
                                    ASIN: ASIN,
                                },
                            });
                    }
                }
            }

            // DETAIL: finding general product info + offers URL
            else if (request.userData.label === "DETAIL") {

                // Find title, url and description of the product
                const title = await page.$eval(
                    'title',
                    (el => el.textContent)
                );

                // Sometimes, the page doesn't contain any description
                const description = await page.$eval(
                    'meta[name^=description]',
                    (el => el != null ? el.getAttribute('content') : '')
                );

                const ASIN = request.userData.ASIN;

                // Query offers
                let offersUrl = new URL(`https://www.amazon.com/gp/offer-listing/${ASIN}`);
                await requestQueue.addRequest(
                    {
                        url: offersUrl.href,
                        userData: {
                            label: "OFFERS",
                            title: title,
                            url: request.url,
                            description: description,
                            ASIN: ASIN,
                        },
                    });

            }

            // OFFERS: Get the product's offers data
            else if (request.userData.label === "OFFERS") {
                let userData = request.userData;

                let offers = await page.$$eval('.olpOffer', elems => {
                    return elems.map((elem) => {
                        let sellerNameElem = elem.querySelector('.olpSellerName > span > a');
                        let sellerName = '';

                        // Sometimes the seller is shown as the brand img, if that's the case, we get the alt of the image as the seller name
                        if (!sellerNameElem) {
                            let res = elem.querySelector('.olpSellerName > img')
                            if (res == null) {
                                sellerName = elem.querySelector('.olpSellerName > img').getAttribute('alt');
                            }

                        } else {
                            sellerName = sellerNameElem.textContent;
                        }
                        const price = elem.querySelector('.olpOfferPrice').textContent.trim();
                        const shippingPrice = 'included in price';
                        return {
                            sellerName,
                            price,
                            shippingPrice,
                        }
                    });
                });

                log.info(`Found ${offers.length} for ASIN ${userData.ASIN}`);
                for (let offer of offers) {
                    let result = {
                        title: userData.title,
                        url: userData.url,
                        description: userData.description,
                        keyword,
                    }
                    Object.assign(result, offer);
                    log.info("Pushing result: ", { result })
                    await Apify.pushData(result);
                }
            }
        },
    });

    log.info('Starting the crawl.');
    await crawler.run();
    log.info('Crawl finished.');
});